package pl.edu.pwsztar.domain.mapper;

import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

public class CreateMovieMapper {

    public Movie MovieDtoToMovieEntity(CreateMovieDto createMovieDto){
        Movie newMovie = new Movie();
        newMovie.setTitle(createMovieDto.getTitle());
        newMovie.setImage(createMovieDto.getImage());
        newMovie.setYear(createMovieDto.getYear());
        return newMovie;
    }

}
